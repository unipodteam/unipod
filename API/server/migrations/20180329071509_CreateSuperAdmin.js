"use strict";

const server = require('../server');
const postgres_ds = server.dataSources.postgres;
const userObj = {
  "first_name": 'super',
  "last_name": 'admin',
  "email": 'superadmin@unipod.com',
  "username": 'superadmin',
  "mobile_no": '1234567890',
  "user_type_name": 'SUPER_ADMIN',
  "password": 'test1234'
};

/**
 * @author Ajaykkumar Rajendran
 */
class CreateSuperAdminMigration {
  // To create a default super-admin user and associate to the role.
  async change() {
    try {
      const role = await server.models.Roles.findOne({where: {name: 'SUPER_ADMIN'}});
      const user = await server.models.Users.create(userObj);
      await server.models.UserRole.create({
        user_id: user.id,
        role_id: role.id,
        principalType: 'USER',
        principalId: user.id,
      });
    } catch (err) {
      throw err;
    }
  }
}

let objCreateSuperAdmin = new CreateSuperAdminMigration();
objCreateSuperAdmin.change();
