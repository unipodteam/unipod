module.exports = {
  foreignKeyConstraint: `ALTER TABLE public.accesstoken ADD CONSTRAINT
  accesstoken_user_id_fk FOREIGN KEY (userId) REFERENCES public.users (id)
  MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION;

  ALTER TABLE public.user_role ADD CONSTRAINT user_role_user_id_fk FOREIGN KEY
  (user_id) REFERENCES public.users (id) MATCH SIMPLE ON UPDATE NO ACTION ON
  DELETE NO ACTION;

  ALTER TABLE public.user_role ADD CONSTRAINT user_role_role_id_fk FOREIGN KEY
  (role_id) REFERENCES public.roles (id) MATCH SIMPLE ON UPDATE NO ACTION ON
  DELETE NO ACTION;`
};
