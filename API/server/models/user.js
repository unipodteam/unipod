'use strict';

module.exports = function(User) {

  User.validatesUniquenessOf('email', {message: 'EmailId already exists'});

  User._invalidateAccessTokensOfUsers = function(userIds, options, cb) {
    if (typeof options === 'function' && cb === undefined) {
      cb = options;
      options = {};
    }

    if (!Array.isArray(userIds) || !userIds.length)
      return process.nextTick(cb);

    var accessTokenRelation = this.relations.accessTokens;
    if (!accessTokenRelation)
      return process.nextTick(cb);

    var AccessToken = accessTokenRelation.modelTo;
    var query = {userId: {inq: userIds}};
    var tokenPK = AccessToken.definition.idName() || 'id';
    if (options.accessToken && tokenPK in options.accessToken) {
      query[tokenPK] = {neq: options.accessToken[tokenPK]};
    }
    // add principalType in AccessToken.query if using polymorphic relations
    // between AccessToken and User
    var relatedUser = AccessToken.relations.user;
    var isRelationPolymorphic = relatedUser && relatedUser.polymorphic &&
      !relatedUser.modelTo;
    if (isRelationPolymorphic) {
      query.principalType = this.modelName;
    }
    return process.nextTick(cb);
    // AccessToken.deleteAll(query, options, cb);
  };
};
